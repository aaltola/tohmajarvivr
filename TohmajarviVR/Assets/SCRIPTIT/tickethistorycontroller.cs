﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tickethistorycontroller : MonoBehaviour {
    private GameObject tickethistory;
    private string filePath;
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    private void Awake()
    {
        trackedObj = GetComponentInParent<SteamVR_TrackedObject>();


        

    }
    // Use this for initialization
    void Start () {
		
        tickethistory = GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().bimhistory;
        filePath = GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().filepath;
    }
	
	// Update is called once per frame
	void Update () {
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {


            tickethistory.SetActive(true);
            tickethistory.transform.rotation = GameObject.FindGameObjectWithTag("MainCamera").transform.rotation;
            tickethistory.transform.position = gameObject.transform.Find("Ticketspawner").transform.position;


           

        }
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) && GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().cameraactivation == true)
        {

            ScreenCapture.CaptureScreenshot(filePath);

            Debug.Log("screenshot");
            GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().activatecamera();

            StartCoroutine(GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().PngToTexture(filePath));
        }
    }
}
