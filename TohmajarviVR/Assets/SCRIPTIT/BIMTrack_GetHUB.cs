﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft;


public class BIMTrack_GetHUB : MonoBehaviour {
    
    
   // public Component issuedata;
   // string HubId = "172";
    //string ProjectId = "291";
    private string get_URL = null;
    private string hub_url = null;
    private string hubname = null;
    /* "https://api.bimtrackbeta.co/v1/hub/byname?name=kissa"*/
    /*bimtoken:"d514d13c1d9e14d046e5e8c0a663449820c10399fa8c9a88f49091e5821189ed"*/
    //hubname=Kissa
    
    public int HubID;
    public int ProjectID;
    public List<BIMTrack_Project> projects;
    public bool getOK;
    List<string> projektinimet = new List<string> { };
    public Dropdown _Project_box;

    // Use this for initialization

    void Start() {
      
     
        
    }

    // Update is called once per frame
    void Update() {


     
     }


    public IEnumerator GetHUB(string Token)
    {

        get_URL = "https://api.bimtrackbeta.co/v2/hubs";
        UnityWebRequest getHub = UnityWebRequest.Get(get_URL);
        getHub.SetRequestHeader("Authorization", "Bearer " + Token);

        yield return getHub.SendWebRequest();
        Debug.Log(getHub.responseCode);

        if (getHub.responseCode >= 200 && getHub.responseCode < 300)
        {
            Debug.Log("Request OK. Statuscode: " + getHub.responseCode);
            Debug.Log(getHub.downloadHandler.text);

            //BIMTrack_Hub hub = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BIMTrack_Hub>>(getHub.downloadHandler.text);
            Newtonsoft.Json.JsonSerializerSettings JSONsetting = new Newtonsoft.Json.JsonSerializerSettings
            {
                DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat,
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore,
                MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore
            };
            List<BIMTrack_Hub> hub = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BIMTrack_Hub>>(getHub.downloadHandler.text, JSONsetting);
            HubID = hub[0].Id;
            Debug.Log(HubID);
            StartCoroutine(GetProjects(Token, HubID));
        }
    }
        public IEnumerator GetProjects(string Token,int hubID)
        {
        hub_url = "https://api.bimtrackbeta.co/v2/hubs/" + hubID + "/projects";
        UnityWebRequest getHub = UnityWebRequest.Get(hub_url);
        getHub.SetRequestHeader("Authorization", "Bearer " + Token);

        yield return getHub.SendWebRequest();
        Debug.Log(getHub.responseCode);







        if (getHub.responseCode >= 200 && getHub.responseCode < 300)
        {

            Newtonsoft.Json.JsonSerializerSettings JSONsetting = new Newtonsoft.Json.JsonSerializerSettings
            {
                DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat,
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore,
                MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore
            };
            List<BIMTrack_Project> projects = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BIMTrack_Project>>(getHub.downloadHandler.text, JSONsetting);

            projektinimet.Clear();
            foreach (BIMTrack_Project project in projects)
            {

                projektinimet.Add(project.Name);
            }

            populateDropdown();

            ProjectID = projects[_Project_box.gameObject.GetComponent<Dropdown>().value].Id;

            gameObject.GetComponent<xml_tarkistusta>().idnaytin();
            getOK = true;
            Debug.Log("Project name: " + projects[0].Name);
            gameObject.GetComponent<xml_tarkistusta>().OK_tarkistus();
        }
        else {
            getOK = false;
            gameObject.GetComponent<xml_tarkistusta>().OK_tarkistus();
        }



    }
        
        

    public void populateDropdown()
    {
        _Project_box.ClearOptions();
        _Project_box.AddOptions(projektinimet);
    }
    
}










