using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
public class BIMTrack_Classes:MonoBehaviour
{

}

[Serializable]
public class BIMTrack_File
{
    public int Id;
    public string Name;
    public int Size;
    public string Extension;
    public DateTime Date;
    public string ThumbnailURL;
    public DateTime ThumbnailURLExpiration;
    public string URL;
    public DateTime URLExpiration;

    public BIMTrack_File()
    {
    }
}

[Serializable]
public class BIMTrack_Attachment
{
    public int Id;
    public string Description;
    public BIMTrack_File File;

    public BIMTrack_Attachment()
    {
    }
}

[Serializable]
public class BIMTrack_Label
{
    public int Id;
    public int ProjectId;
    public string Name;

    public BIMTrack_Label()
    {
    }
}

[Serializable]
public class BIMTrack_ProjectPhaseZone
{
    public int Id;
    public int ProjectId;
    public string Name;
    public string Color;

    public BIMTrack_ProjectPhaseZone()
    {
    }

}

[Serializable]
public class BIMTrack_Priority
{
    public int Id;
    public int ProjectId;
    public string Name;
    public string Color;
    public int Order;

    public BIMTrack_Priority()
    {
    }
}

[Serializable]
public class BIMTrack_ProjectStatus
{
    public int Id;
    public int ProjectId;
    public string Name;
    //public List<BIMTrack_TeamStatus> ProjectTeamIssueStatus;
    public List<BIMTrack_TeamStatus> TeamsAllowedForStatus;
    public string Color;

    public BIMTrack_ProjectStatus()
    {
    }
}

[Serializable]
public class BIMTrack_TeamStatus
{
    public int Id;
    public int ProjectIssueStatusId;
    public int ProjectTeamId;
    public string Name;

    public BIMTrack_TeamStatus()
    {
    }
}

[Serializable]
public class BIMTrack_Type
{
    public int Id;
    public int ProjectId;
    public string Name;
    public string Color;

    public BIMTrack_Type()
    {
    }
}

[Serializable]
public class BIMTrack_Team_2 //looginen virhe bimtrackissa, mahdollisesti joudutaan korjaamaan
{
    public int Id;
    public string Name;
    public BIMTrack_Team_2()
    {
    }
}


[Serializable]
public class BIMTrack_Team
{
    public int Id;
    public int ProjectId;
    public string Name;
    public BIMTrack_Team_2 Team; //confiditiental teams

    public BIMTrack_Team()
    {
    }
}

[Serializable]
public class BIMTrack_TeamList
{
    public int Id;
    public List<BIMTrack_Team> Teams;

    public BIMTrack_TeamList()
    {
    }
}

[Serializable]
public class BIMTrack_Comment
{
    public int Id;
    public int AuthorUserId;
    public string AuthorTitle;
    public DateTime Date;
    public int IssueId;
    public string Value;
    public int LastModificationAuthorId;
    public DateTime LastModificationDate;
    public string LastModificationAuthorTitle;
    public int CommentId;
    public DateTime ArchiveDate;
    public bool IsArchive;
    public string UniqueId;

    public BIMTrack_Comment()
    {
    }
}

[Serializable]
public class BIMTrack_IssueComment
{
    public int Id;
    public int IssueId;
    public int CommentId;
    public BIMTrack_Comment Comment;

    public BIMTrack_IssueComment()
    {
    }
}

[Serializable]
public class BIMTrack_ViewPointComment
{
    public int Id;
    public int ViewPointId;
    public int CommentId;
    public BIMTrack_Comment Comment;

    public BIMTrack_ViewPointComment()
    {
    }
}

[Serializable]
public class BIMTrack_Issue
{
    public int Id;
    public int ProjectId;
    public string Title;
    public int Number;
    public int AssignedToUserId;
    public string Description;
    public string Group;
    public BIMTrack_User Author;
    public BIMTrack_User AssignedTo;
    public BIMTrack_User LastModificationAuthor;
    public DateTime LastModificationDate;
    public DateTime DueDate;
    public DateTime CreationDate;
    public DateTime ClosingDate;
    public List<BIMTrack_Attachment> Attachments;
    public List<BIMTrack_Label> Labels;
    public BIMTrack_ProjectPhaseZone ProjectPhase;
    public BIMTrack_ProjectPhaseZone ProjectZone;
    public BIMTrack_Priority Priority;
    public int PriorityId;
    public BIMTrack_ProjectStatus Status;
    public BIMTrack_Type Type;
    public List<BIMTrack_IssueComment> IssueComments;
    public BIMTrack_Confidentiality Confidentiality;
    public List<BIMTrack_ViewPoint> Viewpoints;
    public List<BIMTrack_Discipline> Disciplines;
    public string CreationSource;
    

    public BIMTrack_Issue()
    {
    }
    
}

public class BIMTrack_Discipline
{
    public int Id;
    public string Name;

    public BIMTrack_Discipline()
    {
    }
}

public class BIMTrack_Confidentiality
{
    public List<BIMTrack_Team> Teams;
    public List<int> TeamIds;


    public BIMTrack_Confidentiality()
    {
    }
}

public class BIMTrack_IssueList
{
    public List<BIMTrack_Issue> IssuesList;

    public BIMTrack_IssueList()
    {
        this.IssuesList = new List<BIMTrack_Issue>();
    }
}


[Serializable]
public class BIMTrack_User
{
    public int Id;
    public string Email;
    public string FirstName;
    public string LastName;
    public DateTime LastWebLogin;
    public DateTime LastAddinLogin;
    public bool IsActivated;

    public BIMTrack_User()
    {
    }
}

[Serializable]
public class BIMTrack_UserRole
{
    public int Id;
    public int Role;
    public BIMTrack_User User;

    public BIMTrack_UserRole()
    {
    }
}

[Serializable]
public class BIMTrack_Snapshot
{
    public int Id;
    public int IssueID;
    public string Name;
    public int Size;
    public string Extension;
    public DateTime Date;
    public string ThumbnailURL;
    public DateTime ThumbnailURLExpiration;
    public string URL;
    public DateTime URLExpiration;

    public BIMTrack_Snapshot()
    {
    }
}

[Serializable]
public class BIMTrack_ProjectSettings
{
    public int DefaultDueDateDays;
    public int ClosedProjectIssueStatusId;
    public int DefaultProjectPhaseId;
    public int DefaultProjectZoneId;
    public int DefaultProjectIssueLabelId;
    public int DefaultProjectIssuePriorityId;
    public int DefaultProjectIssueStatusId;
    public int DefaultProjectIssueTypeId;
    public string DefaultColorAttributeProperty;
    public bool IssueMandatoryAttributesZone;
    public bool IssueMandatoryAttributesPhase;
    public bool IssueMandatoryAttributesAssignedTo;
    public bool IssueMandatoryAttributesPriority;
    public bool IssueMandatoryAttributesLabel;
    public bool IssueMandatoryAttributesTeam;
    public bool IssueMandatoryAttributesIssueGroup;
    public bool IssueMandatoryAttributesDueDate;
    public bool IssueMandatoryAttributesNotify;
    public bool IssueMandatoryAttributesStatus;
    public bool IssueMandatoryAttributesType;

    public BIMTrack_ProjectSettings()
    {
    }

}

[Serializable]
public class BIMTrack_Project
{
    public int HubId;
    public BIMTrack_User Author;
    public BIMTrack_Snapshot Snapshot;
    public DateTime CreationDate;
    public int LastModificationAuthorId;
    public DateTime LastModificationDate;
    public BIMTrack_ProjectSettings ProjectSettings;
    public int Id;
    public string Name;
    public List<BIMTrack_User> Users;
    public List<BIMTrack_Priority> Priorities;
    public List<BIMTrack_Team> Teams;
    public BIMTrack_Project()
    {
    }
}

[Serializable]
public class BIMTrack_SubscriptionPlan
{
    public int Id;
    public bool IsDefault;
    public int IssuesLimit;
    public string Name;
    public string PaymentSubscriptionId;
    public double Price;
    public string SubscriptionType;
    public long UsersLimit;

    public BIMTrack_SubscriptionPlan()
    {
    }
}

[Serializable]
public class BIMTrack_InvoiceLine
{
    public int Id;
    public string Description;
    public int Amount;

    public BIMTrack_InvoiceLine()
    {
    }
}

[Serializable]
public class BIMTrack_Invoice
{
    public int Id;
    public string CardNumber;
    public int CountryTaxAmount;
    public string CountryTaxName;
    public int CountryTaxRate;
    public DateTime CreationDate;
    public string Currency;
    public int CustomerBalance;
    public string SoldToAdressLine1;
    public string SoldToAdressLine2;
    public string SoldToCity;
    public string SoldToCountry;
    public string SoldToName;
    public string SoldToState;
    public string SoldToZip;
    public int StateTaxAmount;
    public string StateTaxName;
    public int StateTaxRate;
    public int SubTotal;
    public int Total;
    public List<BIMTrack_InvoiceLine> InvoiceLines;

    public BIMTrack_Invoice()
    {
    }
}

public class BIMTrack_Hub
{
    public int Id;
    public string Name;
    public BIMTrack_User Author;
    public BIMTrack_Snapshot Snapshot;
    public DateTime CreationDate;
    public List<BIMTrack_Invoice> Invoices;
    public List<BIMTrack_UserRole> UserRoles;
    public List<BIMTrack_Project> Projects;
    public List<BIMTrack_Hub> Templates;
    public BIMTrack_SubscriptionPlan SubscriptionPlan;

    public BIMTrack_Hub()
    {
    }
}



[Serializable]
public class BIMTrack_ViewPoint
{
    public int Id;
    public int IssueId;
    //ViewType possible values: "None", "TwoD" and "ThreeD"
    public string ViewType = "ThreeD";
    public BIMTrack_Snapshot Image;
    public BIMTrack_CameraInformation CameraInformation;
    public BIMTrack_ViewPointViewState ViewPointViewState;
    public List<BIMTrack_ViewPointComment> ViewpointComments;
    public string Source;
    public string ViewName;
    public string ModeleName;
    public BIMTrack_PerspectiveCamera PerspectiveCamera;
    public BIMTrack_OrthogonalCamera OrthogonalCamera;

    public BIMTrack_ViewPoint()
    {
        CameraInformation = new BIMTrack_CameraInformation();
        ViewPointViewState = new BIMTrack_ViewPointViewState();
        ViewpointComments = new List<BIMTrack_ViewPointComment>();
    }
}

[Serializable]
public class BIMTrack_ViewPoint_Post
{
    public int Id;
    public int IssueId;
    //ViewType possible values: "None", "TwoD" and "ThreeD".
    public string ViewType;
    public BIMTrack_PerspectiveCamera PerspectiveCamera;
 //   public BIMTrack_OrthogonalCamera OrthogonalCamera;
    public BIMTrack_ViewpointViewState ViewpointViewState;
    public string ViewName;
    public string ModelName;

    public BIMTrack_ViewPoint_Post()
    {
        this.ViewType = "None";
        this.PerspectiveCamera = new BIMTrack_PerspectiveCamera();
       // this.OrthogonalCamera = new BIMTrack_OrthogonalCamera();
        this.ViewpointViewState = new BIMTrack_ViewpointViewState();
    }
}

[Serializable]
public class BIMTrack_PerspectiveCamera
{
    public BIMTrack_Cartensian CameraPosition;
    public BIMTrack_Cartensian CameraDirection;
    public BIMTrack_Cartensian CameraUpVector;
    public double FieldOfView;

    public BIMTrack_PerspectiveCamera()
    {
        this.CameraPosition = new BIMTrack_Cartensian();
        this.CameraDirection = new BIMTrack_Cartensian();
        this.CameraUpVector = new BIMTrack_Cartensian();
    }
}


[Serializable]
public class BIMTrack_OrthogonalCamera
{
    public BIMTrack_Cartensian CameraPosition;
    public BIMTrack_Cartensian CameraDirection;
    public BIMTrack_Cartensian CameraUpVector;
    public int ViewToWorldScale;

    public BIMTrack_OrthogonalCamera()
    {
        this.CameraPosition = new BIMTrack_Cartensian();
        this.CameraDirection = new BIMTrack_Cartensian();
        this.CameraUpVector = new BIMTrack_Cartensian();
    }
}

[Serializable]
public class BIMTrack_Element
{
    public string IfcGuid;
    public string OriginatingSystem;
    public string AuthoringToolId;
}

[Serializable]
public class BIMTrack_SectionBox
{
    public BIMTrack_Cartensian Center;
    public BIMTrack_Cartensian Rotation;
    public BIMTrack_Cartensian Size;

    public BIMTrack_SectionBox()
    {
        this.Center = new BIMTrack_Cartensian();
        this.Rotation = new BIMTrack_Cartensian();
        this.Size = new BIMTrack_Cartensian();
    }
}

[Serializable]
public class BIMTrack_Cartensian
{
    public double X;
    public double Y;
    public double Z;
}

[Serializable]
public class BIMTrack_ClippingPlane
{
    public BIMTrack_Cartensian Location;
    public BIMTrack_Cartensian Direction;

    public BIMTrack_ClippingPlane()
    {
        this.Location = new BIMTrack_Cartensian();
        this.Direction = new BIMTrack_Cartensian();
    }
}

[Serializable]
public class BIMTrack_ViewpointViewState
{
    public List<BIMTrack_Element> HiddenElements;
    public List<BIMTrack_Element> SelectedElements;
    public string ClipMode;
    public BIMTrack_SectionBox SectionBox;
    public List<BIMTrack_ClippingPlane> ClippingPlanes;

    public BIMTrack_ViewpointViewState()
    {
        this.HiddenElements = new List<BIMTrack_Element>();
        this.SelectedElements = new List<BIMTrack_Element>();
        this.ClipMode = "None";
        this.SectionBox = new BIMTrack_SectionBox();
        this.ClippingPlanes = new List<BIMTrack_ClippingPlane>();
    }

}


[Serializable]
public class BIMTrack_CameraInformation
{
    //CameraViewType possible values: "Undefined", "Orthogonal" and "Perspective"
    public string CameraViewType;
    //Degrees
    public int FieldOfView;
    // X, Y, Z
    public BIMTrack_Point Position;
    public BIMTrack_Point Direction;
    public BIMTrack_Point UpVector;
    public int OrthoHeigth;
    public int OrthoWidth;

    public BIMTrack_CameraInformation()
    {
        Position = new BIMTrack_Point();
        Direction = new BIMTrack_Point();
        UpVector = new BIMTrack_Point();
    }
}

[Serializable]
public class BIMTrack_ClipPlane
{
    public BIMTrack_Point Location;
    public BIMTrack_Point Direction;

    public BIMTrack_ClipPlane()
    {
    }
}
[Serializable]
public class BIMTrack_ViewPointViewState
{
    public int HiddenElementsId;
    public int SelectedElementsId;
    public BIMTrack_SectionBox SectionBox;
    public string ClipMode;
    public List<BIMTrack_ClipPlane> ClipPlanes;

    public BIMTrack_ViewPointViewState()
    {
    }
}
[Serializable]
//3D space coordinates
public class BIMTrack_Point
{
    public double X;
    public double Y;
    public double Z;

    public BIMTrack_Point()
    {
        this.X = 0.0;
        this.Y = 0.0;
        this.Z = 0.0;
    }
}

public class BIMTrack_CartensianPoint
{
    public double X;
    public double Y;
    public double Z;

    public BIMTrack_CartensianPoint()
    {
        this.X = 0.0;
        this.Y = 0.0;
        this.Z = 0.0;
    }
}
