﻿
using UnityEngine;

public class Kasi : MonoBehaviour {
    public int Currentkasi = 0;
    bool handussa= false;
    
    

    void Start () {
        SelectKasi();
	}
    private SteamVR_TrackedObject trackedObj;
        
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    void Awake()
    {
        trackedObj = GetComponentInParent<SteamVR_TrackedObject>();
       
    }

    void Update () {
        if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad))     //osoitus sormi
        {
            Currentkasi = 3;
            SelectKasi();
        }
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad))     //osoitus sormi pois
        {
            Currentkasi = 0;
            SelectKasi(); 
        }

        if (Controller.GetHairTriggerDown())
        {
            
            handussa = true;
            Currentkasi = 2;
            SelectKasi();
        }
        if (Controller.GetHairTriggerUp())
        {
            handussa = false;
            Currentkasi = 0;
            SelectKasi();
        }
	}
    
    void OnTriggerEnter(Collider  other) //käsi saapuu tartuttavaan objektiin
    {
        if (handussa==false&&other.GetComponent < Rigidbody >()== true && other.tag=="grab"||other.tag=="grabnophysics" )
        {
            
           
        Currentkasi = 1;
        SelectKasi();
        }
       
    }
    
    private void OnTriggerStay(Collider other)//käsi on tartuttavan objektin sisällä
    {
        if(other.tag == "grab" || other.tag == "grabnophysics"&& other.GetComponent<Rigidbody>() == true)
        {

       
        if (handussa == false )
        {
            Currentkasi = 1;
            SelectKasi();
        }
        }
    }
    void OnTriggerExit(Collider other)//käsi lähtee tartuttavan objektin vierestä
    {
        Currentkasi = 0;
        SelectKasi();


    }
    void SelectKasi()//valitaan oikea käsimalli lapsiobjekteista
    {
        int i = 0;
        foreach (Transform item in transform)
        {
            if (i == Currentkasi)
                item.gameObject.SetActive(true);
            else
                item.gameObject.SetActive(false);
            i++;
        }
    }
}
