﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BIMTrack_PostFile : MonoBehaviour
{

    /*
---------
POST FILE
---------
    IMAGES: .PNG
    MODEL FILES: .IFC

    Content-Type: multipart/form-data

    Input the file as binarydata to WWWForm.
    Set fieldname to "file" and give it the proper filename with file extension.
*/

    public GameObject bimticket;
    private string postFileUrl;
    public string filePath = "Assets/TohmaScreenShot.png";
    



    // Use this for initialization
    void Start()
    {
        bimticket = gameObject.GetComponent<ReceiveText>().bimticket;
    }
    //https://api.bimtrackbeta.co/v1/hub/123/project/123/issue/123/viewpoints/123/snapshot


   public IEnumerator PostFile(int HubID,int projectID, int issueID,int viewpointID, string Bimtoken)
    {
        postFileUrl = "https://api.bimtrackbeta.co/v1/hub/"+HubID+"/project/"+projectID+"/issue/"+issueID+ "/viewpoints/"+viewpointID+"/snapshot";
        byte[] file = File.ReadAllBytes(filePath);
        string filename = Path.GetFileName(filePath);

        WWWForm form = new WWWForm();
        form.AddBinaryData("file", file, filename);

        UnityWebRequest PostFile = UnityWebRequest.Post(postFileUrl, form);
        PostFile.SetRequestHeader("Authorization", "Bearer " + Bimtoken);

        // DO NOT DEFINE THE FOLLOWING! HTTP ERROR 400 OR 415
        // uwr.SetRequestHeader("Content-Type", "multipart/form-data");

        yield return PostFile.SendWebRequest();

        if (PostFile.responseCode >= 200 && PostFile.responseCode < 300)
        {
            Debug.Log("File received, responsecode: " + PostFile.responseCode);
            Debug.Log(PostFile.downloadHandler.text);
           
        }
        else
        {
            Debug.Log("Failure, responsecode: " + PostFile.responseCode);
            Debug.Log(PostFile.downloadHandler.text);
        }
    }
}