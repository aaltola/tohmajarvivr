﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ReceiveText : MonoBehaviour {
    public string filepath;
    //"C:/Users/KW2/Desktop/tohmajärviprojektibuilded/tohmis.png";
    public string texttoadd;
    public InputField texttarget;
    public bool cameraactivation;
    public GameObject bimticket;
    public GameObject ticketcontroller;
    public GameObject näppis;
    public GameObject bimhistory;
    public int caretposition;
    public int caretmover;
    Texture photo;

    private GameObject viewpointticket;
    private GameObject numberkeys;
    private int HubID;
    private int projectID;
    public string Bimtoken;
    private string Title;
    private string Description;
    private int priority;
    private int userid;
    private int teamid;
    public TextAsset imageAsset;
    // Use this for initialization
    void Awake () {
        

        viewpointticket = GameObject.FindGameObjectWithTag("BimTicket").transform.Find("BimTicket+").gameObject;
        bimticket = GameObject.FindGameObjectWithTag("BimTicket").transform.parent.gameObject;
        numberkeys= GameObject.FindGameObjectWithTag("Näppis").transform.Find("Lisämerkit").gameObject;
        näppis = GameObject.FindGameObjectWithTag("Näppis").transform.parent.gameObject;
        bimhistory = GameObject.FindGameObjectWithTag("BIMhistory").transform.parent.gameObject;
        bimticket.SetActive(false);
        näppis.SetActive(false);
        bimhistory.SetActive(false);

        filepath = Application.dataPath + "/tohmis.png";
        Debug.Log(filepath);

    }

    

   
    public void postissue()
    {
        bimticket.transform.Find("BimTicket/Canvasticket/Responcecode").gameObject.GetComponent<Text>().text = "POSTING...";
        bimticket.transform.Find("BimTicket/Canvasticket/Responcecode").gameObject.GetComponent<Text>().color = Color.yellow;
        bimticket.transform.Find("BimTicket/Canvasticket/PostIssue").GetComponent<Button>().interactable = false;
        HubID = gameObject.GetComponent<XML_lukija>().HubID;
        projectID = gameObject.GetComponent<XML_lukija>().projectID;
        Bimtoken = gameObject.GetComponent<XML_lukija>().tokenBIM;
        Title = bimticket.transform.Find("BimTicket/Canvasticket/Title").GetComponent<InputField>().text;
        Description = bimticket.transform.Find("BimTicket/Canvasticket/Description/Viewport/Content/InputFieldMod").GetComponent<InputField>().text;
        priority = gameObject.GetComponent<BIMTrack_GetProject>().priorityorder;
        userid = gameObject.GetComponent<BIMTrack_GetProject>().userID;
        
        teamid = gameObject.GetComponent<BIMTrack_GetProject>().tiimiID;

        StartCoroutine(gameObject.GetComponent<BIMTrack_Post_Issue>().PostIssue(HubID, projectID, Bimtoken,Title,Description,priority,userid,teamid));
       

    }
    public void allowpost(float slider)
    {
        
        if (slider >= 0.9)
        {
            
           bimticket.transform.Find("BimTicket/Canvasticket/PostIssue").GetComponent<Button>().interactable = true;
            return;
        }
        else
        {
            
           bimticket.transform.Find("BimTicket/Canvasticket/PostIssue").GetComponent<Button>().interactable = false;
            return;
        }
    }
    public void Scalekeyboard(float slider)
    {
        näppis.transform.localScale += new Vector3(slider, slider, 0);//laita skaalautumaan oikeassa suhteessa ja pienennä max/min
    }
    public void activatecamera()
    {
        
        if (cameraactivation == true)
        {

            GameObject.FindGameObjectWithTag("MainCamera").gameObject.transform.Find("Cameraon").gameObject.SetActive(false);
            cameraactivation = false;
            return;
        }
        else {
            
            cameraactivation = true;
            GameObject.FindGameObjectWithTag("MainCamera").gameObject.transform.Find("Cameraon").gameObject.SetActive(true);
            return;
        }
        
    }
  
   public IEnumerator PngToTexture(string filepath)
    {
        yield return new WaitForSeconds(5);

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filepath))
        {
            fileData = File.ReadAllBytes(filepath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            bimticket.transform.Find("BimTicket/BimTicket+").gameObject.GetComponent<Renderer>().material.mainTexture = tex;
            gameObject.GetComponent<BIMTrack_PostIssueViewPoint>().imagetaken = true;
            Debug.Log("kuva lissätty");
        }

    }


    public void viewpointactivation()
    {
        
        if (bimticket.transform.Find("BimTicket/Canvasticket/ToggleViewpointpost").GetComponent<Toggle>().isOn==true)
        {
           
            viewpointticket.SetActive(true);
            return;
        }
        if (bimticket.transform.Find("BimTicket/Canvasticket/ToggleViewpointpost").GetComponent<Toggle>().isOn == false)
        {
            
            viewpointticket.SetActive(false);
            return;
        }
    }



    public void numbers()
    {
        
        if (numberkeys.gameObject.activeSelf==true)
        {

            numberkeys.gameObject.SetActive(false);    
            return;
        }
        if (numberkeys.gameObject.activeSelf == false)
        {

            numberkeys.gameObject.SetActive(true);
            return;
        }
    }
    public void gettext(string texter)
    {
        texttoadd = texter;
        
       texttarget.text = texttarget.text.Insert(texttarget.caretPosition, texttoadd);
       texttarget.caretPosition = texttarget.caretPosition + 1;
       
        Debug.Log("caretpos :" + texttarget.caretPosition);
    }
    public void selectedtextbox(InputField target)

    {
        
        if (näppis.activeSelf == false)
        {
        näppis.SetActive(true);
            näppis.transform.position = bimticket.transform.Find("BimTicket/Keyboardspawner").transform.position;
        näppis.transform.rotation = GameObject.FindGameObjectWithTag("MainCamera").transform.rotation;

        }
        texttarget = target;
        bimticket.transform.Find("BimTicket/Canvasticket/Responcecode").gameObject.GetComponent<Text>().text = "";


        caretmover = 0;
    }
    public void fullerase()
    {
        caretmover = texttarget.caretPosition - texttarget.selectionAnchorPosition;
        if (caretmover < 0)
        {
           caretmover = caretmover * -1;
            texttarget.text = texttarget.text.Remove(texttarget.caretPosition, caretmover);
        }
        else
        {

        
        texttarget.text = texttarget.text.Remove(texttarget.selectionAnchorPosition,caretmover);
        }
    }
    public void erasetext()
    {
        if (texttarget.text.Length > 0)
        {
            texttarget.text = texttarget.text.Substring(0, texttarget.text.Length - 1);
        }
    }
    public void caps()
    {
        if (näppis.transform.Find("Näppis/Päämerkit/pienetkirjaimet").gameObject.activeSelf == true)
        {
            näppis.transform.Find("Näppis/Päämerkit/pienetkirjaimet").gameObject.SetActive(false);
            näppis.transform.Find("Näppis/Päämerkit/Isotkirjaimet").gameObject.SetActive(true);
            return;
        }
        else
        {
            näppis.transform.Find("Näppis/Päämerkit/pienetkirjaimet").gameObject.SetActive(true);
            näppis.transform.Find("Näppis/Päämerkit/Isotkirjaimet").gameObject.SetActive(false);
        }
    }
   
}
