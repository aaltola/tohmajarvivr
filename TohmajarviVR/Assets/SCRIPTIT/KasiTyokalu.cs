﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KasiTyokalu : MonoBehaviour {
   
   
    
    private double raycastdist;

    public Transform cameraRigTransform;
    public GameObject teleportReticlePrefab;
    private GameObject reticle;
    private Transform teleportReticleTransform;
    public Transform headTransform;
    public Vector3 teleportReticleOffset;
    public LayerMask teleportMask;
    private bool shouldTeleport;
    private SteamVR_TrackedObject trackedObj;
    public GameObject laserPrefab;
    private GameObject laser;
    private Transform laserTransform;
    private Vector3 hitPoint;
    private GameObject collidingObject;
    private GameObject objectInHand;
    private bool triggerhold;
    public float throwforce=1;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int) trackedObj.index); }
    }
   
    void Awake()
    {
        trackedObj = GetComponentInParent<SteamVR_TrackedObject>();
        
    }
    private void Start()
    {
        reticle = Instantiate(teleportReticlePrefab);
       
        teleportReticleTransform = reticle.transform;

        laser = Instantiate(laserPrefab);
        
        laserTransform = laser.transform;
    }

    void Update()

    {
        
            if (Controller.GetPress(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            RaycastHit hit;

            // 2
            if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, teleportMask))
            {
                hitPoint = hit.point;
                ShowLaser(hit);
        // 1
        reticle.SetActive(true);
        // 2
        teleportReticleTransform.position = hitPoint + teleportReticleOffset;
        // 3
        shouldTeleport = true;
            }
        }
        else // 3
        {
            laser.SetActive(false);
            reticle.SetActive(false);
        }
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu) && shouldTeleport)
        {
            Teleport();
        }
    
        if (Controller.GetHairTriggerDown())
        {
            if (collidingObject)
            {
                GrabObject();
            }
             
        }

        // 2
        if (Controller.GetHairTriggerUp())
        {
            if (objectInHand &&objectInHand.tag == "grabnophysics")
            {
                Releasenophysobject();
            }
            if (objectInHand)
            {
                ReleaseObject();
            }
             
        }
    }
    private void SetCollidingObject(Collider col)
    {
        if (col.tag == "grab"||col.tag=="grabnophysics")
        {
            // 1
            if (collidingObject || !col.GetComponent<Rigidbody>())
            {
                return;
            }
            // 2
            collidingObject = col.gameObject;
        }
    }
    

    // 1
    public void OnTriggerEnter(Collider other)
    {
        SetCollidingObject(other);
       
    }

    // 2
    public void OnTriggerStay(Collider other)
    {
        SetCollidingObject(other);
       
    }

    // 3
    public void OnTriggerExit(Collider other)
    {
       
        
        if (!collidingObject)
        {
            return;
        }
        
        collidingObject = null;
    }

    private void GrabObject()
    {
        // 1
        objectInHand = collidingObject;
        collidingObject = null;
        // 2
        var joint = AddFixedJoint();                                    
        joint.connectedBody = objectInHand.GetComponent<Rigidbody>();
    }

    // 3
    private FixedJoint AddFixedJoint()
    {
        FixedJoint fx = gameObject.AddComponent<FixedJoint>();
        fx.breakForce = 20000;
        fx.breakTorque = 20000;
        return fx;
    }

    private void Releasenophysobject()
    {
        if (GetComponent<FixedJoint>() && GetComponent<Rigidbody>() != null)
        {
            // 2
            GetComponent<FixedJoint>().connectedBody = null;
            Destroy(GetComponent<FixedJoint>());
        }
        // 4
        objectInHand = null;
    }
    private void ReleaseObject()
    {
        // 1
        if (GetComponent<FixedJoint>() && GetComponent<Rigidbody>()!=null)
        {
            // 2
            GetComponent<FixedJoint>().connectedBody = null;
            Destroy(GetComponent<FixedJoint>());
            // 3
            objectInHand.GetComponent<Rigidbody>().velocity = Controller.velocity*throwforce;
            

           
            objectInHand.GetComponent<Rigidbody>().angularVelocity = Controller.angularVelocity ;
        }
        // 4
        objectInHand = null;
    }
    private void Teleport()
    {
        // 1
        shouldTeleport = false;
        // 2
        reticle.SetActive(false);
        // 3
        Vector3 difference = cameraRigTransform.position - headTransform.position;
        // 4
        difference.y = 0;
        // 5

        if (raycastdist > 0.5)
        {

        cameraRigTransform.position = hitPoint + difference;
        }
    }
    private void ShowLaser(RaycastHit hit)
    {
        // 1
        laser.SetActive(true);
        // 2

        laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, 0.5f);
        // 3
        laserTransform.LookAt(hitPoint);
        // 4
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laser.transform.localScale.y,
            hit.distance);
        raycastdist = hit.distance;
    }
}
