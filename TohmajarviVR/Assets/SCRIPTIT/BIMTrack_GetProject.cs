﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BIMTrack_GetProject : MonoBehaviour {
    private int hubID;
    private int projectID;
    private string token;
    private string GetProjectUrl;
    //https://api.bimtrackbeta.co/v1/hub/123/project/data?projectId=123
    public BIMTrack_Project project;
    public Dropdown _users;
    public Dropdown _priority;
    public Dropdown _confidentials;
    

    public int userID;
    public int priorityorder;
    public int tiimiID;
    public List<string> userit = new List<string> { };
    public List <string> priorityt = new List<string> { };
    public List<Sprite> prioritytcolors = new List<Sprite> { };
    public List<string> tiimit = new List<string> { };
    public List<Dropdown.OptionData> priorities = new List<Dropdown.OptionData>();



    private void Start()
    {
        hubID = gameObject.GetComponent<XML_lukija>().HubID;
        projectID = gameObject.GetComponent<XML_lukija>().projectID;
        token = gameObject.GetComponent<XML_lukija>().tokenBIM;


        StartCoroutine(GetProject());
    }


    public IEnumerator GetProject()
    {

        GetProjectUrl = "https://api.bimtrackbeta.co/v2/hubs/"+hubID+"/projects/"+projectID;
        Debug.Log(GetProjectUrl);


        UnityWebRequest GetProjects = UnityWebRequest.Get(GetProjectUrl);
        GetProjects.SetRequestHeader("Authorization", "Bearer " + token);


        yield return GetProjects.SendWebRequest();

        if (GetProjects.responseCode >= 200 && GetProjects.responseCode < 300)
        {
            Newtonsoft.Json.JsonSerializerSettings JSONsetting = new Newtonsoft.Json.JsonSerializerSettings
            {
                DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat,
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore,
                MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore
            };

            
            project = Newtonsoft.Json.JsonConvert.DeserializeObject<BIMTrack_Project>(GetProjects.downloadHandler.text, JSONsetting);
            userit.Add("Assign to user");
            foreach (BIMTrack_User user in project.Users)
            {

                userit.Add(user.Email);
            }

            foreach (BIMTrack_Priority prio in project.Priorities)
            {

               

                var asd = new Dropdown.OptionData(prio.Name, stringtosprite(prio.Color));
                priorities.Add(asd);
                

            }




            tiimit.Add("Assign to team");
            foreach (BIMTrack_Team tem in project.Teams)
            {

                tiimit.Add(tem.Name);
            }
            populateDropdown();
            
            Debug.Log("Request OK. Statuscode: " + GetProjects.responseCode);
            //Debug.Log(GetProjects.downloadHandler.text);
            //Debug.Log("projet" + GetProjects.downloadHandler.text);




        }
        else
        {
            Debug.Log(GetProjects.responseCode);
        }





    }
    public void chooseuser(int userorder) {
        if (userorder == 0)
        {
            userID = 0;
        }

        if (userorder > 0)
        {

        userID = project.Users[userorder-1].Id;
        }

    }
    public void choosepriority(int order)
    {
        priorityorder = project.Priorities[order].Id;


    }
    public void chooseteam(int orderi)
    {
        if (orderi == 0)
        {
            tiimiID = 0;
        }
        if (orderi > 0)
        {

            tiimiID = project.Teams[orderi - 1].Id;
            Debug.Log(tiimiID);
        }
    }




public void populateDropdown()
    {
        _priority.ClearOptions();
        _users.ClearOptions();
        _confidentials.ClearOptions();


        _users.AddOptions(userit);
        _confidentials.AddOptions(tiimit);
        _priority.AddOptions(priorities);


    }

    public Sprite stringtosprite(string colorstring)
    {
        Color asd;
        Texture2D textu = new Texture2D(1,1);
        ColorUtility.TryParseHtmlString(colorstring,out asd);
        Color32 col32 = asd;
        
        textu.SetPixel(0,0,asd);
        textu.Apply();
        Debug.Log(col32);
        Sprite spr = new Sprite();
        //spr = Sprite.Create(textu, new Rect(0.0f, 0.0f, textu.width, textu.height), new Vector2(0.5f, 0.5f), 100.0f);
        spr = Sprite.Create(textu, new Rect(0.0f, 0.0f, textu.width, textu.height), new Vector2(0.5f, 0.5f), 100.0f);
    

        
        return spr;
    }

   
}
