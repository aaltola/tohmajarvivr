﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class XML_lukija : MonoBehaviour {

    string xml_tiedostoNimi = "BIMjaWatson.xml";
    private XML_luokka Tiedot;
    public string tokenBIM;
    public string userWatson;
    public string passWatson;
    public int HubID;
    public int projectID;

    // Use this for initialization
    void Start () {
        XML_lukemista();
        gameObject.GetComponent<BIMTrack_GetProject>().enabled=true;
        gameObject.GetComponent<WatsonStreaming>().enabled = true;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void XML_lukemista()
    {
        Tiedot = null;
        XmlSerializer serializer = new XmlSerializer(typeof(XML_luokka));
        StreamReader reader = new StreamReader(xml_tiedostoNimi);
        Tiedot = (XML_luokka)serializer.Deserialize(reader);
        reader.Close();
        tokenBIM = Tiedot.tokenBIM;
        userWatson = Tiedot.tokenWatson;
        passWatson = Tiedot.passwordWatson;
        HubID = Tiedot.HubID;
        projectID = Tiedot.projectID;
}
}
