﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;

public class ColorWheel : MonoBehaviour {
    public GameObject colorCube;
    private float hue, saturnation, value = 1f;
    public GameObject blackwheel;
    public GameObject pallero;
    public bool Colorwheelinuse;

	void Start () {
        if (GetComponentInParent<VRTK_ControllerEvents>() == null)
        {
            Debug.LogError("need VRTK controllerevents");
        }
        blackwheel = transform.Find("CanvasHolder/Canvas/BlackWheel").gameObject;
       

        GetComponentInParent<VRTK_ControllerEvents>().TouchpadAxisChanged += new ControllerInteractionEventHandler(DoTouchpadAxisChanged);
        GetComponentInParent<VRTK_ControllerEvents>().ButtonTwoPressed += new ControllerInteractionEventHandler(linewidth);
    }
    private void OnEnable()
    {
        Colorwheelinuse = true;
    }
    private void OnDisable()
    {
        Colorwheelinuse = false;
    }
    private void linewidth(object sender, ControllerInteractionEventArgs e)
    {
        if (Colorwheelinuse == true)
        {
        float linewidth= gameObject.GetComponent<piirto>().Linewidth;
        if (GetComponentInParent<VRTK_ControllerEvents>().buttonTwoPressed)
        {

             linewidth = linewidth +0.01f;
        }    
        gameObject.GetComponent<piirto>().Linewidth = linewidth;
        }
    }
    private void DoTouchpadAxisChanged(object sender, ControllerInteractionEventArgs e)
    {
        if (Colorwheelinuse == true){

        if (GetComponentInParent<VRTK_ControllerEvents>().touchpadPressed)
        {
            ChangedValue(e.touchpadAxis);
        }
        else {
        ChangeHueSaturation(e.touchpadAxis, e.touchpadAngle);
        }
        }
    }

    private void ChangedValue(Vector2 touchpadAxis)
    {
        this.value = (touchpadAxis.y + 1) / 2;
        
        Color currColor = blackwheel.GetComponent<Image>().color;
        currColor.a = 1 - this.value;
        blackwheel.GetComponent<Image>().color = currColor;

        UpdateColor();
        
    }

   

    private void ChangeHueSaturation(Vector2 touchpadAxis, float touchpadAngle)
    {
        float normalAngle = touchpadAngle - 90;
        if (normalAngle < 0)
            normalAngle = 360 + normalAngle;

        //Debug.Log("axis: " + touchpadAxis + "angle: " +normalAngle);

        float rads = normalAngle * Mathf.PI / 180;
        float maxX = Mathf.Cos(rads);
        float maxY = Mathf.Sin(rads);

       float currX = touchpadAxis.x;
       float currY = touchpadAxis.y;

        

        float percentX = Mathf.Abs(currX / maxX);
        float percentY = Mathf.Abs(currY / maxY);

        this.hue = normalAngle / 360.0f;
        this.saturnation = (percentX + percentY) / 2;

        //Debug.Log("hue: "+ this.hue + "saturation: " + this.saturnation);
        
        palleroupdate(currX, currY);
        UpdateColor();
        
    }

    private void UpdateColor() {
        Color color = Color.HSVToRGB(this.hue, this.saturnation, this.value);
        colorCube.GetComponent<Renderer>().material.color = color;
        pallero.GetComponent<Renderer>().material.color = color;

    }
    private void palleroupdate(float x, float y)
    {
       
        pallero.transform.localPosition = new Vector2(x,y);
    }
	void Update () {
		
	}
}
