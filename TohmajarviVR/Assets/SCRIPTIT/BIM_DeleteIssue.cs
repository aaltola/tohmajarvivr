﻿using CI.HttpClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class BIM_DeleteIssue : MonoBehaviour
{


    private int issueID;
    string post_url = null;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public IEnumerator ArchiveIssue(int HubID, int ProjectID, int issueID, string token)
    {
        string arc_url = "https://api.bimtrackbeta.co/v2/hubs/" + HubID + "/projects/" + ProjectID + "/issues/" + issueID;
        byte[] message = System.Text.Encoding.UTF8.GetBytes("{\"ArchiveIssue\": true}");

        UnityWebRequest uwr = UnityWebRequest.Put(arc_url, message);
        uwr.method = "PATCH";
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.SetRequestHeader("Authorization", "Bearer " + token);

        yield return uwr.SendWebRequest();

        if (uwr.responseCode >= 200 && uwr.responseCode < 300)
        {
            Debug.Log("Archive succeeded, responsecode: " + uwr.responseCode);
            Debug.Log(uwr.downloadHandler.text);
            gameObject.GetComponent<BIMTrack_GETProjectIssues>().startGetHUBIssues();
                
        }
        else
        {
            Debug.Log("Archive failure, responsecode: " + uwr.responseCode);
            Debug.Log(uwr.downloadHandler.text);
        }
    }
}