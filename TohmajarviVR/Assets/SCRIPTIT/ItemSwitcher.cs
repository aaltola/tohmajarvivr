﻿
using UnityEngine;

public class ItemSwitcher : MonoBehaviour {
    private int itemtoshaderize;
   
    public int CurrentItem = 0;
    private GameObject bimtickethistory;
    private GameObject bimticket;
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    void Awake()
    {
        trackedObj = GetComponentInParent<SteamVR_TrackedObject>();

    }

    void Start () {
        
        SelectItem();
        


    }
	
   
    void OnTriggerEnter(Collider target)
    {
        
        int previtem = CurrentItem;
        if (target.tag == "Item1"&& Controller.GetPress(SteamVR_Controller.ButtonMask.Grip)==true)//Defalut handtoolin valinta
        {
            CurrentItem = 0;
            
        }
        if (target.tag == "Item2" && Controller.GetPress(SteamVR_Controller.ButtonMask.Grip) == true)//Tickettool
        {
            CurrentItem = 1;

        }
        if (target.tag == "Item3" && Controller.GetPress(SteamVR_Controller.ButtonMask.Grip) == true)//tickethistorytool
        {
            CurrentItem = 2;
         
        }
        if (target.tag == "Item4" && Controller.GetPress(SteamVR_Controller.ButtonMask.Grip) == true)//kynatool
        {
            CurrentItem = 3;
            
        }
        if (target.tag == "Item5" && Controller.GetPress(SteamVR_Controller.ButtonMask.Grip) == true)//itemspawner
        {
            CurrentItem = 4;
          
        }
        if (target.tag == "Item6" && Controller.GetPress(SteamVR_Controller.ButtonMask.Grip) == true)//empty
        {
            CurrentItem = 5;
          
        }
        if (previtem!= CurrentItem)
        {

        SelectItem();
        }
    }
    void SelectItem()
    {
        int i = 0;
        foreach (Transform item in transform)
        {
            if (i == CurrentItem)
            {
                item.gameObject.SetActive(true);
              
            }
            else
                item.gameObject.SetActive(false);
                
             i++;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        
    }
   
}
