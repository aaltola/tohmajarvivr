﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BIMTrack_PostIssueViewPoint : MonoBehaviour
{

    // Use this for initialization
    public bool imagetaken=false;
    private string postIssueViewPoint_URL;
    public int viewPointID;
    private GameObject bimticket;
    private string path;

    JsonSerializerSettings jsonsettings = new JsonSerializerSettings
    {
        DateFormatHandling = DateFormatHandling.IsoDateFormat,
        NullValueHandling = NullValueHandling.Ignore,
        DefaultValueHandling = DefaultValueHandling.Ignore,
        MissingMemberHandling = MissingMemberHandling.Ignore
    };

    void Start()
    {

        bimticket = gameObject.GetComponent<ReceiveText>().bimticket;
        path = gameObject.GetComponent<ReceiveText>().filepath;
        

    }

    // Update is called once per frame
    void Update()
    {

    }
    //https://api.bimtrackbeta.co/v1/hub/123/project/123/issue/123/viewpoints


   public IEnumerator PostIssueViewPoint(int HubID, int projectID,int issueID,string Bimtoken,float x, float y,float z)
    {
        BIMTrack_ViewPoint point = new BIMTrack_ViewPoint();
        point.ViewType = "ThreeD";
        point.PerspectiveCamera = new BIMTrack_PerspectiveCamera();
        point.PerspectiveCamera.FieldOfView = 30;

        point.PerspectiveCamera.CameraDirection.X = 0.99;
        point.PerspectiveCamera.CameraDirection.Y = 0.99;
        point.PerspectiveCamera.CameraDirection.Z = 0.1;

        point.PerspectiveCamera.CameraPosition.X = x;
        point.PerspectiveCamera.CameraPosition.Y = z;
        point.PerspectiveCamera.CameraPosition.Z = y;

        point.PerspectiveCamera.CameraUpVector.X = 0.50;
        point.PerspectiveCamera.CameraUpVector.Y = 0.50;
        point.PerspectiveCamera.CameraUpVector.Z = 0.00;

        string json = JsonConvert.SerializeObject(point, jsonsettings);

        string viewPointImgUrl = "https://api.bimtrackbeta.co/v2/hubs/" + HubID + "/projects/" + projectID + "/issues/" + issueID + "/viewpoints";
        byte[] viewPoint = System.Text.Encoding.UTF8.GetBytes(json);
       
        

        byte[] file = File.ReadAllBytes(path);
        string filename = Path.GetFileName(path);//mieti parempiratkaisu

        WWWForm form = new WWWForm();

        form.AddBinaryData("viewPointMetadata", viewPoint, "viewPoint.json", "application/json");
        if (imagetaken==true)
        {
        form.AddBinaryData("file", file, filename, "application/octet-stream");

        }

        

        
        UnityWebRequest uwr = UnityWebRequest.Post(viewPointImgUrl, form);
        uwr.SetRequestHeader("Authorization", "Bearer " + Bimtoken);

        yield return uwr.SendWebRequest();

        if (uwr.responseCode >= 200 && uwr.responseCode < 300)
        {
            Debug.Log("File received, responsecode: " + uwr.responseCode);
            bimticket.transform.Find("BimTicket/BimTicket+").gameObject.GetComponent<Renderer>().material.mainTexture = null;
            bimticket.transform.Find("BimTicket/Canvasticket/ToggleViewpointpost").gameObject.GetComponent<Toggle>().isOn = false;
            imagetaken = false;
            Debug.Log(uwr.downloadHandler.text);
        }
        else
        {
            Debug.Log("Failure, responsecode: " + uwr.responseCode);
            Debug.Log(uwr.downloadHandler.text);
        }

    }

}