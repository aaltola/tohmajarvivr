﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class cammanualculling : MonoBehaviour {
    private Camera cam;
    public float[] dis;
    public bool update=false;
	// Use this for initialization
	void Start () {
        cam = Camera.main;
        cam.layerCullDistances = dis;
	}
	
	// Update is called once per frame
	void Update () {
        if (update == true)
        {
            update = false;
            cam.layerCullDistances = dis;
        }
	}
}
