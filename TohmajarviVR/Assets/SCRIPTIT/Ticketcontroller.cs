﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using VRTK;

public class Ticketcontroller : MonoBehaviour
{

    // Use this for initialization
    public LayerMask mask=5;
    RaycastHit hit;
    public GameObject ticket;
    public GameObject Näppis;
    public bool camera=false;
    
    private string filePath;

    private int HubID;
    private int projectID;
    private string Bimtoken;

    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    private void Awake()
    {
        trackedObj = GetComponentInParent<SteamVR_TrackedObject>();
       

        ticket = GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().bimticket;
        Näppis = GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().näppis;

    }


    void Start()
    {
        
        
        HubID = GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<XML_lukija>().HubID;
        projectID = GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<XML_lukija>().projectID;
        Bimtoken = GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<XML_lukija>().tokenBIM;


        filePath = GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().filepath;

}


    // Update is called once per frame
            //   StartCoroutine(GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<BIMTrack_Post_Issue>().PostIssue(HubID,projectID,Bimtoken));
    void Update()
    {



        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad)&&GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().cameraactivation==true)
        {
            
            ScreenCapture.CaptureScreenshot(filePath);

            Debug.Log("screenshot");
            GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().activatecamera();
            
            StartCoroutine(GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().PngToTexture(filePath));
        }
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
           
                
                ticket.SetActive(true);
                ticket.transform.rotation = GameObject.FindGameObjectWithTag("MainCamera").transform.rotation;
                ticket.transform.position = gameObject.transform.Find("Ticketspawner").transform.position;
                      
            }
           
            

        
        if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);
            Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit,100f,mask);

                

           
                if (hit.collider.gameObject.GetComponent<InputField>()==true)
                {
                    
                    Debug.Log("rayray");

                GameObject.FindGameObjectWithTag("ticketmaster").GetComponent<ReceiveText>().selectedtextbox(hit.collider.gameObject.GetComponent<InputField>());
            }
            
               
            }
            }


           /* if (e.currentTarget.GetComponent<UnityEngine.UI.InputField>().enabled == true)
             {
                 GameObject.FindGameObjectWithTag("BimTicket").GetComponent<ReceiveText>().texttarget = e.currentTarget.GetComponent<UnityEngine.UI.InputField>();
             }*/
        }







    


