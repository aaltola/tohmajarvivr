﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ItemSpawner : MonoBehaviour {

    private SteamVR_TrackedObject trackedObj;
    public int nytItemi = 0;
    public int maxs;
    private int muxs;
    GameObject spawnattava;
    public bool deleteactivate = false;


    void Awake()
    {
        trackedObj = GetComponentInParent<SteamVR_TrackedObject>();

    }

    // Use this for initialization

    void Start()
    {
        SelectItem();
        maxs = muxs;
        if (GetComponentInParent<VRTK_ControllerEvents>() == null)
        {
            Debug.LogError("need VRTK controllerevents");
        }

        GetComponentInParent<VRTK_ControllerEvents>().TouchpadPressed += new ControllerInteractionEventHandler(activeitemswitcher);
        GetComponentInParent<VRTK_ControllerEvents>().TriggerClicked += new ControllerInteractionEventHandler(dropItem);
        GetComponentInParent<VRTK_ControllerEvents>().ButtonTwoPressed += new ControllerInteractionEventHandler(deleteitem);

    }

        // Update is called once per frame
        void Update () {

		
	}
    void deleteitem(object sender, ControllerInteractionEventArgs e)
    {
        Debug.Log("buttonpressed");
        if (deleteactivate == false)
        {
         deleteactivate = true;
            return;
        }
        if(deleteactivate == true)
        {           
            deleteactivate = false;
            return;
        }
    }
    void activeitemswitcher(object sender, ControllerInteractionEventArgs e)
    {
        if(GetComponentInParent<ItemSwitcher>().CurrentItem == 4)
        {
            if (e.touchpadAngle < 180 && e.touchpadAngle > 0)
            {
                nytItemi = nytItemi + 1;
                Debug.Log(nytItemi);
            }
            if (e.touchpadAngle > 180 && e.touchpadAngle < 360)
            {
                nytItemi = nytItemi - 1;
                Debug.Log(nytItemi);
            }
            if (nytItemi >= maxs)
            {
                nytItemi = 0;
            }
            if (nytItemi < 0)
            {
                nytItemi = maxs - 1;
            }
            SelectItem();
        }
    }


    void SelectItem()
    {
        int i = 0;
        
        foreach (Transform item in transform)
        {
            if (i == nytItemi)
                item.gameObject.SetActive(true);
            
            else
                item.gameObject.SetActive(false);
            i++;
            muxs++;
        }
    }

    void dropItem(object sender, ControllerInteractionEventArgs e)
    {
        if (GetComponentInParent<ItemSwitcher>().CurrentItem == 4)
        {
            GameObject spawn = new GameObject();
            

            


          spawn =  Instantiate(gameObject.transform.GetChild(nytItemi).gameObject, trackedObj.transform.position, Quaternion.identity);
            spawn.transform.localScale = new Vector3(3F, 3f, 3f);
            spawn.AddComponent<Rigidbody>();
            spawn.AddComponent<BoxCollider>();
            spawn.AddComponent<SphereCollider>().isTrigger=true;
        }
            
        
    }
}
