﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Valikko : MonoBehaviour {

    GameObject val;
    public Shader standardshader;
    public Shader highlightshader;
    private SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }
    void Awake()
    {
        trackedObj = gameObject.GetComponent<SteamVR_TrackedObject>();
        
             val = transform.Find("Valikko").gameObject;
    }
    void Start () {
      
    }
	
	// Update is called once per frame
	void Update () {
        if (Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
         
            val.gameObject.transform.parent = null;
            val.gameObject.transform.rotation = GameObject.FindGameObjectWithTag("MainCamera").transform.rotation;
            val.gameObject.SetActive(true);
            
            
        }

        
        if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
        {


            val.gameObject.SetActive(false);
            val.transform.parent = gameObject.transform;
            val.transform.position = gameObject.transform.position;
            
            foreach (Transform item in transform.Find("Valikko").transform)
            {
                item.gameObject.GetComponent<Renderer>().material.shader = standardshader;
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (val.gameObject.activeSelf==true&&other.transform.IsChildOf(val.transform))
        {
            other.gameObject.GetComponent<Renderer>().material.shader = highlightshader;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (val.gameObject.activeSelf == true && other.transform.IsChildOf(val.transform))
        {
            other.gameObject.GetComponent<Renderer>().material.shader = highlightshader;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (val.gameObject.activeSelf == true && other.transform.IsChildOf(val.transform))
        {
            other.gameObject.GetComponent<Renderer>().material.shader = standardshader;
        }
    }




}
