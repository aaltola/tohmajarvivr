﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BIMTrack_Post_Issue : MonoBehaviour {


    private GameObject bimticket;
    public int issueID;
    private float xcoordinate, ycoordinate, zcoordinate;
    string post_url =null ;
   // "https://api.bimtrackbeta.co/v1/hub/172/project/291/issue/add"

        // uus projektin id on 332

    // Use this for initialization
    void Start () {
        bimticket = gameObject.GetComponent<ReceiveText>().bimticket;
    }

    // Update is called once per frame
    void Update () {
       

    }




    public IEnumerator PostIssue(int HubID, int ProjectID, string Bimtoken, string Title, string Description, int priority, int user, int teamid)
    {

        post_url = "https://api.bimtrackbeta.co/v2/hubs/" + HubID + "/projects/" + ProjectID + "/issues";

        Debug.Log(teamid);
        BIMTrack_Issue issue = new BIMTrack_Issue();
        //Debug.Log(Title + "     " + Description);
        issue.Title = Title;
        issue.Description = Description;

        if (user != 0)
        {
        issue.AssignedToUserId = user;
        }
        if (priority!=0)
        {
        issue.PriorityId = priority;
        }

        if (teamid != 0)
        {

            issue.Confidentiality = new BIMTrack_Confidentiality();
            issue.Confidentiality.TeamIds = new List<int>();
            issue.Confidentiality.TeamIds.Add(teamid);
        }
        



      

        UnityWebRequest request = new UnityWebRequest(post_url, "POST");

        Newtonsoft.Json.JsonSerializerSettings JSONsetting = new Newtonsoft.Json.JsonSerializerSettings
        {
            DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat,
            NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
            DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore,
            MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore
        };

        string json = Newtonsoft.Json.JsonConvert.SerializeObject(issue, JSONsetting);
        byte[] bodyRaw = Encoding.UTF8.GetBytes(json);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "Bearer " + Bimtoken);

        yield return request.SendWebRequest();


        Debug.Log(Bimtoken);
        Debug.Log(json);


        Debug.Log("Status Code: " + request.responseCode);

        if (request.responseCode >= 200 && request.responseCode < 300)
        {
            httpresponce(request.responseCode);


            if (GameObject.FindGameObjectWithTag("BimTicket").transform.Find("Canvasticket/ToggleViewpointpost").GetComponent<Toggle>().isOn == true)
            {

                BIMTrack_Issue issue2 = Newtonsoft.Json.JsonConvert.DeserializeObject<BIMTrack_Issue>(request.downloadHandler.text, JSONsetting);
                //syotetaan issuen ID muuttujaan:
                issueID = issue2.Id;
                Debug.Log("ISSUE ID: " + issueID);
              xcoordinate = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Coordinates>().finalx;
              ycoordinate = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Coordinates>().finaly;
              zcoordinate = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Coordinates>().finalz;


                StartCoroutine(gameObject.GetComponent<BIMTrack_PostIssueViewPoint>().PostIssueViewPoint(HubID, ProjectID, issueID, Bimtoken,xcoordinate,ycoordinate,zcoordinate));

            }

        }
        else
        {
            httpresponce(request.responseCode);
        }






    }
    void httpresponce(long responce)
    {
        Text httpresponce = bimticket.transform.Find("BimTicket/Canvasticket/Responcecode").gameObject.GetComponent<Text>();
        if (responce>= 200 && responce < 300)
        {
            httpresponce.text = "Post OK";
            httpresponce.color = Color.green;
            clearticket();
            return;
        }
        if (responce == 401)
        {
            httpresponce.text = "UNAUTHORIZED";
            httpresponce.color = Color.red;
        }
        else {
            httpresponce.text = "http error code: "+responce;
            httpresponce.color = Color.red;
        }



          
    }
    void clearticket()
    {
        InputField title = bimticket.transform.Find("BimTicket/Canvasticket/Title").gameObject.GetComponent<InputField>();
        InputField Description = bimticket.transform.Find("BimTicket/Canvasticket/Description/Viewport/Content/InputFieldMod").gameObject.GetComponent<InputField>();

        bimticket.transform.Find("BimTicket/Canvasticket/Postallowslider").gameObject.GetComponent<Slider>().value = 0;
        bimticket.transform.Find("BimTicket/Canvasticket/Prioritydropdown").gameObject.GetComponent<Dropdown>().value = 0;
        bimticket.transform.Find("BimTicket/Canvasticket/Confidentiality").gameObject.GetComponent<Dropdown>().value = 0;
        bimticket.transform.Find("BimTicket/Canvasticket/Assignedtouserdropdown").gameObject.GetComponent<Dropdown>().value = 0;


        title.text= title.text.Substring(0, title.text.Length - title.text.Length);
        Description.text = Description.text.Substring(0, Description.text.Length - Description.text.Length);
        

    }
}
