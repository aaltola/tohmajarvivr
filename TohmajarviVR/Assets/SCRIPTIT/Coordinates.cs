﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coordinates : MonoBehaviour {
    public float x, y, z;
    public float convertedx, convertedy, convertedz;
    public float offsetx, offsety, offsetz;
    public float convertx, converty, convertz;
    public float rotatex,rotatey,rotatez;
    public float rotateoffsetx,rotateoffsety,rotateoffsetz;
    public float rotateconvertedx, rotateconvertedy, rotateconvertedz;
    public float rotateconvertx, rotateconverty, rotateconvertz;
    public float rotateradianx,rotateradiany,rotateradianz;
    public float finalx, finaly, finalz;
         
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        x = gameObject.transform.position.x;
        y = gameObject.transform.position.y;
        z = gameObject.transform.position.z;
        convertedx = (x * convertx) + offsetx;
        convertedy = (y * converty) + offsety;
        convertedz = (z * convertz) + offsetz;

        finalx = convertedx * -1;
        finaly = convertedy;
        finalz = convertedz * -1;



        /*rotatex = gameObject.transform.eulerAngles.x;
        rotatey = gameObject.transform.eulerAngles.y;
        rotatez = gameObject.transform.eulerAngles.z;
        rotateconvertedx = rotatex*rotateconvertx + rotateoffsetx;
        rotateconvertedy = rotatey*rotateconverty + rotateoffsety;
        rotateconvertedz = rotatez*rotateconvertz + rotateoffsetz;
        rotateradianx = rotateconvertedx * Mathf.Deg2Rad;
        rotateradiany = rotateconvertedy * Mathf.Deg2Rad;
        rotateradianz = rotateconvertedz * Mathf.Deg2Rad;*/


    }
}
