﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class piirto : MonoBehaviour
{
    public Material lmat;
    public SteamVR_TrackedObject trackedObj;
    public float Linewidth=0.01f;
    private MeshLineRenderer currLine;
    private int numClicks = 0;

    // Update is called once per frame
    private void OnEnable()
    {
        Linewidth = 0.01f;
        
    }
    private void Awake()
    {
        trackedObj = GetComponentInParent<SteamVR_TrackedObject>();
    }
    void Update()
    {
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)trackedObj.index);
        if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
        {
        lmat = GetComponent<Renderer>().material;
            GameObject go = new GameObject();
           
            currLine = go.AddComponent<MeshLineRenderer>();
           

            go.AddComponent<MeshRenderer>();
            //go.AddComponent<MeshFilter>();

            currLine.lmat = lmat;
            

            currLine.setWidth(Linewidth);

            numClicks = 0;
        }
        else if (device.GetTouch(SteamVR_Controller.ButtonMask.Trigger))
        {
            //currLine.SetVertexCount(numClicks + 1);
            //currLine.SetPosition(numClicks, trackedObj.transform.position);

            currLine.AddPoint(gameObject.transform.position);
            numClicks++;
        }
    }
}
