﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BIMTrack_GETProjectIssues : MonoBehaviour
{
    public bool viewpoint;
    private string get_Issues_URL;
    private string get_Issue_URL;
    private int hubID;
    private int projectID;
    private string BimToken;
    private Dropdown _Ticket_History;
    private Text _IssueDesc;
    public List<string> Titles = new List<string> { };
    public List<BIMTrack_Issue> issues = new List<BIMTrack_Issue> { };
    public BIMTrack_Issue issue;
    private string imageurl;
    private int issueid;
    public double xtp, ytp, ztp;
    private bool tpallow=false;

    // Use this for initialization
    void Start()
    {
        hubID = gameObject.GetComponent<XML_lukija>().HubID;
        projectID = gameObject.GetComponent<XML_lukija>().projectID;
        BimToken = gameObject.GetComponent<XML_lukija>().tokenBIM;


        // StartCoroutine(getHUBIssues);
    }

    // Update is called once per frame
    void Update()
    {

    }

    // https://api.bimtrackbeta.co/v1/hub/123/project/123/issues?sort=Ascending&startRecord=1&returnCount=12&assignedToUserId=123&priorityId=123&statusId=123
    // https://api.bimtrackbeta.co/v1/hub/+hubID+/project/+projectID+/issues?sort=Ascending
    //Assingto/priority/Statusfilte on optionaalisia


    public void startGetHUBIssues()
    {
        
        _Ticket_History = GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/Ticket_History").GetComponent<Dropdown>();
        _IssueDesc = GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/Scroll View/Viewport/Content/IssueDesc").GetComponent<Text>();

        _Ticket_History.value = 0;
        _IssueDesc.text = null;

        GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/Issuecolor").GetComponent<Image>().sprite = null;
        GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/Issuecolor/IssuePriority").GetComponent<Text>().text = null;
        GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/IssueAssUser").GetComponent<Text>().text = null;
        GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/IssueAssTeam").GetComponent<Text>().text = null;

        GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("previewimage").GetComponent<Renderer>().material =null;
        GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("previewimage").GetComponent<Renderer>().material.color = Color.white;

        StartCoroutine(GetHUBIssues(hubID, projectID, BimToken));
       
        
    }


    IEnumerator GetHUBIssues(int HubID,int projectID,string Bimtoken)
    {
        get_Issues_URL = "https://api.bimtrackbeta.co/v2/hubs/" + HubID + "/projects/" + projectID + "/issues?sort=-Number&startRecord=1&returnCount=25";
        //    https://api.bimtrackbeta.co/v2/hubs/172/projects/332/issues?sort=-Number&startRecord=1&returnCount=100
        Debug.Log(get_Issues_URL);
        UnityWebRequest GetHUBIssues = UnityWebRequest.Get(get_Issues_URL);
        GetHUBIssues.SetRequestHeader("Authorization", "Bearer " + Bimtoken);

        Newtonsoft.Json.JsonSerializerSettings JSONsetting = new Newtonsoft.Json.JsonSerializerSettings
        {
            DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat,
            NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
            DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore,
            MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore
        };

        string json = Newtonsoft.Json.JsonConvert.SerializeObject(GetHUBIssues, JSONsetting);

        yield return GetHUBIssues.SendWebRequest();
        Debug.Log(GetHUBIssues.responseCode);
        if (GetHUBIssues.responseCode >= 200 && GetHUBIssues.responseCode < 300)
        {
            Debug.Log("Request OK. Statuscode: " + GetHUBIssues.responseCode);
            


            /*
            //Tämä seuraava pitää olla, koska JsonUtility ei tykkää jos JSON-taulukolla ei ole nimeä.
            //https://forum.unity.com/threads/how-to-load-an-array-with-jsonutility.375735/
            string json = "{\"IssuesList\":" + GetHUBIssues.downloadHandler.text + "}";
            */
            // List <BIMTrack_Issue> 
            issues = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BIMTrack_Issue>>(GetHUBIssues.downloadHandler.text, JSONsetting);
            Titles.Clear();
            Titles.Add("[SELECT ISSUE]");
            foreach (BIMTrack_Issue issue in issues)
            {
               // Debug.Log(issue.Title);
                Titles.Add(issue.Title);
            }
            
            populateDropdown_issues();

        }
    }

    public void populateDropdown_issues()
    {

        _Ticket_History.ClearOptions();
        _Ticket_History.AddOptions(Titles);

    }

    public void populateIssueDescription(int order)
    {

        if (order > 0)
        {

            _IssueDesc.text = issues[order-1].Description;
        }
    }

    public void getissueid(int order)
    {
      
        if (order > 0)
        {

            issueid = issues[order-1].Id;
            Debug.Log(issueid);
            StartCoroutine(GetHUBIssue(hubID, projectID, BimToken, issueid));
        }
    }

    public void allowarchiveissue(float a)
    {
        if (a > 0.95f)
        {GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/DeleteButton").GetComponent<Button>().interactable = true;}
        else { GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/DeleteButton").GetComponent<Button>().interactable = false;}
    }

    public void archiveissue()
    {

        issueid = issues[GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/Ticket_History").GetComponent<Dropdown>().value - 1].Id;
        StartCoroutine(gameObject.GetComponent<BIM_DeleteIssue>().ArchiveIssue(hubID,projectID, issueid,BimToken));
        GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/Allowdeleteslider").GetComponent<Slider>().value = 0;
    }

    public void selectedHistoryIssue(int orderi)
    {
        if (orderi > 0)
        {
            Debug.Log("Valittu orderi: " + orderi);

            //Set priority text
           

            try
            {
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/Issuecolor/IssuePriority").GetComponent<Text>().text = issues[orderi - 1].Priority.Name;
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/Issuecolor").GetComponent<Image>().sprite = gameObject.GetComponent<BIMTrack_GetProject>().stringtosprite(issues[orderi - 1].Priority.Color);
            }
            catch (ArgumentOutOfRangeException aaa)
            {
                Debug.Log(aaa.Message);
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/IssuePriority").GetComponent<Text>().text = "[Not set]";
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/Issuecolor").GetComponent<Image>().sprite = gameObject.GetComponent<BIMTrack_GetProject>().stringtosprite(issues[orderi - 1].Priority.Color);
            }
            catch (NullReferenceException nuller)
            {
                Debug.Log(nuller.Message);
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/IssuePriority").GetComponent<Text>().text = "[Not set]";
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/Issuecolor").GetComponent<Image>().sprite = gameObject.GetComponent<BIMTrack_GetProject>().stringtosprite(issues[orderi - 1].Priority.Color);
            }
            //Set assigned user text
            try
            {
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/IssueAssUser").GetComponent<Text>().text = issues[orderi - 1].AssignedTo.Email;
            }
            catch (ArgumentOutOfRangeException ooo)
            {
                Debug.Log(ooo.Message);
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/IssueAssUser").GetComponent<Text>().text = "[Not set]";
            }
            catch (NullReferenceException nuller)
            {
                Debug.Log(nuller.Message);
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/IssueAssUser").GetComponent<Text>().text = "[Not set]";
            }

            //Set team text
            try
            {
               
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/IssueAssTeam").GetComponent<Text>().text = issues[orderi - 1].Confidentiality.Teams[0].Team.Name;
            } catch (ArgumentOutOfRangeException aor)
            {
                Debug.Log(aor.Message);
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/IssueAssTeam").GetComponent<Text>().text = "[Not set]";
            }
            catch (NullReferenceException nuller)
            {
                Debug.Log(nuller.Message);
                GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("Canvasticket/IssueAssTeam").GetComponent<Text>().text = "[Not set]";
            }

        }
    }

    IEnumerator GetHUBIssue(int HubID, int projectID, string Bimtoken, int issueid)
    {
        get_Issue_URL = "https://api.bimtrackbeta.co/v2/hubs/" + HubID + "/projects/" + projectID + "/issues/"+issueid;
        
        Debug.Log(get_Issue_URL);
        UnityWebRequest GetHUBIssue = UnityWebRequest.Get(get_Issue_URL);
        GetHUBIssue.SetRequestHeader("Authorization", "Bearer " + Bimtoken);

        Newtonsoft.Json.JsonSerializerSettings JSONsetting = new Newtonsoft.Json.JsonSerializerSettings
        {
            DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat,
            NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
            DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore,
            MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore
        };

        string json = Newtonsoft.Json.JsonConvert.SerializeObject(GetHUBIssue, JSONsetting);

        yield return GetHUBIssue.SendWebRequest();
        Debug.Log(GetHUBIssue.responseCode);
        if (GetHUBIssue.responseCode >= 200 && GetHUBIssue.responseCode < 300)
        {
            // Debug.Log("Request OK. Statuscode: " + GetHUBIssue.responseCode);
            // Debug.Log(GetHUBIssue.downloadHandler.text);

            issue = Newtonsoft.Json.JsonConvert.DeserializeObject<BIMTrack_Issue>(GetHUBIssue.downloadHandler.text, JSONsetting);
            Debug.Log(issue.Confidentiality);
            try
            {
                xtp = -issue.Viewpoints[0].PerspectiveCamera.CameraPosition.X + 674.11;
                ytp = issue.Viewpoints[0].PerspectiveCamera.CameraPosition.Z + 5;
                ztp = -issue.Viewpoints[0].PerspectiveCamera.CameraPosition.Y + 100;
                Debug.Log(xtp);
                Debug.Log(ytp);
                Debug.Log(ztp);
                if (xtp == 674.11 & ytp == 5 & ztp == 100)
                {
                    viewpoint = false;
                }
                else
                {
                    viewpoint = true;
                }
                imageurl = issue.Viewpoints[0].Image.URL;
                Debug.Log("IMAGE URL: "+ imageurl);
                StartCoroutine(GetIssueImage(imageurl));

            }
            catch(Exception)
            {

                StartCoroutine(Imageerror());
            }
            

           
        }
    }


    public void tptocoordinates(float slider)
    {
        if (slider > 0.95f&&viewpoint==true)
        {

            Vector3 tppoint = new Vector3((float)xtp, (float)ytp-1.163f,(float)ztp);
           
            
            GameObject.FindGameObjectWithTag("SteamvrCamerarig").gameObject.transform.position =tppoint;
            viewpoint = false;
            gameObject.GetComponent<ReceiveText>().bimhistory.transform.position = Camera.main.transform.Find("Ticketspawner").position;
            gameObject.GetComponent<ReceiveText>().bimhistory.transform.rotation = Camera.main.transform.rotation;
            gameObject.GetComponent<ReceiveText>().bimhistory.transform.Find("BimHistory/Canvasticket/Teleporttoissueslider").GetComponent<Slider>().value = 0;

        }
    }






   
    IEnumerator GetIssueImage(string imageurli)
    {
        Texture2D tex;
        tex = new Texture2D(4, 4, TextureFormat.DXT1, false);
        using (WWW www = new WWW(imageurli))
        {
            yield return www;
            www.LoadImageIntoTexture(tex);
            GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("previewimage").GetComponent<Renderer>().material.mainTexture = tex;
        }
    }
    IEnumerator Imageerror()
    {
        yield return new WaitForSeconds(1);
        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists("Assets/NOPIC.png"))
        {
            fileData = File.ReadAllBytes("Assets/NOPIC.png");
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            GameObject.FindGameObjectWithTag("BIMhistory").transform.Find("previewimage").GetComponent<Renderer>().material.mainTexture = tex;
            Debug.Log("kuva lissätty");
        }

    }
}