//ViewPoint postausta varten. JsonUtility.ToJson generoi ViewPointin oikein
// Ossimies



/*using System;
using System.Collections.Generic;

public class ViewPoint
{
    //ViewType possible values: "None", "TwoD" and "ThreeD"
    public string ViewType;
    public CameraInformation CameraInformation;
    public ViewPointViewState ViewPointViewState;

    public ViewPoint()
    {
        this.ViewType = "None";
        this.CameraInformation = new CameraInformation();
        this.ViewPointViewState = new ViewPointViewState();
    }
}
[Serializable]
public class CameraInformation
{
    //CameraViewType possible values: "Undefined", "Orthogonal" and "Perspective"
    public string CameraViewType;
    //Degrees
    public int FieldOfView;
    // X, Y, Z
    public Point Position;
    public Point Direction;
    public Point UpVector;
    public int OrthoHeigth;
    public int OrthoWidth;

    public CameraInformation()
    {
        this.CameraViewType = "Undefined";
        this.FieldOfView = 90;
        this.Position = new Point();
        this.Direction = new Point();
        this.UpVector = new Point();
        this.OrthoHeigth = 0;
        this.OrthoWidth = 0;
    }
}

[Serializable]
public class SectionBox
{
    public Point Center;
    public Point Rotation;
    public Point Size;

    public SectionBox()
    {
        this.Center = new Point();
        this.Rotation = new Point();
        this.Size = new Point();
    }
}
[Serializable]
public class ClipPlane
{
    public Point Location;
    public Point Direction;

    public ClipPlane()
    {
        this.Location = new Point();
        this.Direction = new Point();
    }
}
[Serializable]
public class ViewPointViewState
{
    public int HiddenElementsId;
    public int SelectedElementsId;
    public SectionBox SectionBox;
    public string ClipMode;
    public List<ClipPlane> ClipPlanes;

    public ViewPointViewState()
    {
        this.HiddenElementsId = 0;
        this.SelectedElementsId = 0;
        this.SectionBox = new SectionBox();
        this.ClipMode = "None";
        this.ClipPlanes = new List<ClipPlane>();
    }
}
[Serializable]
//3D space coordinates
public class Point
{
    public double X;
    public double Y;
    public double Z;

    public Point()
    {
        this.X = 0;
        this.Y = 0;
        this.Z = 0;
    }
}*/
