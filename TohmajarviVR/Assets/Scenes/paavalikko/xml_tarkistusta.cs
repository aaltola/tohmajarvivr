﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Linq;

public class xml_tarkistusta : MonoBehaviour {

    string xml_tiedostoNimi = "BIMjaWatson.xml";
    private XML_luokka Tiedot;

    public  InputField _BIM_tokeni;
    public InputField _Watson_tokeni;
    //public InputField _HUB_name_box;
    public InputField _Watson_password;
    private int HubID;
    private int ProjectID;
    private bool getOK=false;
    private List<BIMTrack_Project> testi;



    public void Start()
    {
        
        GameObject.Find("BIMtoken_OK").gameObject.GetComponent<Text>().enabled = false;
        GameObject.Find("WatsonToken_OK").gameObject.GetComponent<Text>().enabled = false;
        //GameObject.Find("BIMhub_OK").gameObject.GetComponent<Text>().enabled = false;
        GameObject.Find("Watson_Pass_OK").gameObject.GetComponent<Text>().enabled = false;
        //GameObject.Find("Project_box").gameObject.GetComponent<>



        XML_lukeminen();
    }

    public void XML_lukeminen() {

        if (File.Exists(xml_tiedostoNimi))
        {
            XML_luku();
            StartCoroutine(gameObject.GetComponent<BIMTrack_GetHUB>().GetHUB(_BIM_tokeni.text));

        }
        else
        {
            XML_luonti();
        }



        OK_tarkistus();
    }
    public void idnaytin()
    {
        HubID = gameObject.GetComponent<BIMTrack_GetHUB>().HubID;
        ProjectID = gameObject.GetComponent<BIMTrack_GetHUB>().ProjectID;


        GameObject.Find("ID").gameObject.GetComponent<Text>().text = "HUBID: " + HubID.ToString() + " ProjectID: " + ProjectID.ToString();
    }
    public void XML_paivitys()
    {
        StartCoroutine(gameObject.GetComponent<BIMTrack_GetHUB>().GetHUB(_BIM_tokeni.text));
        _BIM_tokeni = GameObject.FindGameObjectWithTag("Bimtoken").GetComponent<InputField>();
        _Watson_tokeni = GameObject.FindGameObjectWithTag("Watsonitokeni").GetComponent<InputField>();
       // _HUB_name_box = GameObject.FindGameObjectWithTag("hubiname").GetComponent<InputField>();
        _Watson_password = GameObject.FindGameObjectWithTag("passWatson").GetComponent<InputField>();
        HubID = gameObject.GetComponent<BIMTrack_GetHUB>().HubID;
        ProjectID = gameObject.GetComponent<BIMTrack_GetHUB>().ProjectID;

       
        GameObject.Find("ID").gameObject.GetComponent<Text>().text ="HUBID: "+ HubID.ToString()+" ProjectID: "+ProjectID.ToString();

        Tiedot.muokkaa(_BIM_tokeni.text, _Watson_tokeni.text, HubID, ProjectID, _Watson_password.text);
        XmlSerializer serializer = new XmlSerializer(typeof(XML_luokka));
        using (StreamWriter streamWriter = new StreamWriter(xml_tiedostoNimi))
        {
            serializer.Serialize(streamWriter, Tiedot);
        }
        OK_tarkistus();
    }

    private void XML_luonti()
    {
        Tiedot = new XML_luokka();

        
        XmlSerializer serializer = new XmlSerializer(typeof(XML_luokka));
        using (StreamWriter streamWriter = new StreamWriter(xml_tiedostoNimi))
        {
            serializer.Serialize(streamWriter, Tiedot);
        }

        _BIM_tokeni = GameObject.FindGameObjectWithTag("Bimtoken").GetComponent<InputField>();
        _Watson_tokeni = GameObject.FindGameObjectWithTag("Watsonitokeni").GetComponent<InputField>();
       // _HUB_name_box = GameObject.FindGameObjectWithTag("hubiname").GetComponent<InputField>();
        _Watson_password = GameObject.FindGameObjectWithTag("passWatson").GetComponent<InputField>();

        _BIM_tokeni.text = Tiedot.tokenBIM.ToString();
        _Watson_tokeni.text = Tiedot.tokenWatson.ToString();
        //_HUB_name_box.text = Tiedot.HubName.ToString();
        _Watson_password.text = Tiedot.passwordWatson.ToString();
    }

    private void XML_luku()
    {


        Tiedot = null;
        XmlSerializer serializer = new XmlSerializer(typeof(XML_luokka));
        StreamReader reader = new StreamReader(xml_tiedostoNimi);
        Tiedot = (XML_luokka)serializer.Deserialize(reader);
        reader.Close();

        _BIM_tokeni = GameObject.FindGameObjectWithTag("Bimtoken").GetComponent<InputField>();
        _Watson_tokeni = GameObject.FindGameObjectWithTag("Watsonitokeni").GetComponent<InputField>();
        //_HUB_name_box = GameObject.FindGameObjectWithTag("hubiname").GetComponent<InputField>();
        _Watson_password = GameObject.FindGameObjectWithTag("passWatson").GetComponent<InputField>();

        getOK = gameObject.GetComponent<BIMTrack_GetHUB>().getOK;

        _BIM_tokeni.text = Tiedot.tokenBIM.ToString();
        _Watson_tokeni.text = Tiedot.tokenWatson.ToString();
        //_HUB_name_box.text = Tiedot.HubName.ToString();
        _Watson_password.text = Tiedot.passwordWatson.ToString();

        //testi = gameObject.GetComponent<BIMTrack_GetHUB>().projects;

        //List<string> lista = new List<string> { testi[0].Name };
        //Debug.Log("aa: "+testi[0].Name);
        //_Project_box.ClearOptions();
        //_Project_box.AddOptions(lista);
    }



    public void OK_tarkistus()
    {
        
        _BIM_tokeni = GameObject.FindGameObjectWithTag("Bimtoken").GetComponent<InputField>();
        _Watson_tokeni = GameObject.FindGameObjectWithTag("Watsonitokeni").GetComponent<InputField>();
        //_HUB_name_box = GameObject.FindGameObjectWithTag("hubiname").GetComponent<InputField>();
        _BIM_tokeni.text = Tiedot.tokenBIM.ToString();
        _Watson_tokeni.text = Tiedot.tokenWatson.ToString();
        //_HUB_name_box.text = Tiedot.HubName.ToString();
        getOK = gameObject.GetComponent<BIMTrack_GetHUB>().getOK;

        if (getOK==true)
        {
            GameObject.Find("BIMtoken_OK").gameObject.GetComponent<Text>().enabled = true;
            GameObject.Find("BIMtoken_notOK").gameObject.GetComponent<Text>().enabled = false;
        }
        else {
            GameObject.Find("BIMtoken_OK").gameObject.GetComponent<Text>().enabled = false;
            GameObject.Find("BIMtoken_notOK").gameObject.GetComponent<Text>().enabled = true;
        }

        if (getOK==true)
        {
            //GameObject.Find("BIMhub_OK").gameObject.GetComponent<Text>().enabled = true;
            //GameObject.Find("BIMhub_notOK").gameObject.GetComponent<Text>().enabled = false;
        }
        else
        {
            //GameObject.Find("BIMhub_OK").gameObject.GetComponent<Text>().enabled = false;
            //GameObject.Find("BIMhub_notOK").gameObject.GetComponent<Text>().enabled = true;
        }

        if (_Watson_tokeni.text.Length == 36)
        {
            GameObject.Find("WatsonToken_OK").gameObject.GetComponent<Text>().enabled = true;
            GameObject.Find("WatsonToken_notOK").gameObject.GetComponent<Text>().enabled = false;
        }
        else
        {
            GameObject.Find("WatsonToken_OK").gameObject.GetComponent<Text>().enabled = false;
            GameObject.Find("WatsonToken_notOK").gameObject.GetComponent<Text>().enabled = true;
        }

        if (_Watson_password.text.Length == 12)
        {
            GameObject.Find("Watson_Pass_OK").gameObject.GetComponent<Text>().enabled = true;
            GameObject.Find("Watson_Pass_notOK").gameObject.GetComponent<Text>().enabled = false;
        }
        else
        {
            GameObject.Find("Watson_Pass_OK").gameObject.GetComponent<Text>().enabled = false;
            GameObject.Find("Watson_Pass_notOK").gameObject.GetComponent<Text>().enabled = true;
        }

    }

    public void seuraavaScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void infoButton()
    {
        if (gameObject.transform.Find("Infocanvas").gameObject.activeSelf)
        {
            gameObject.transform.Find("Infocanvas").gameObject.SetActive(false);
        }
        else
        {
            gameObject.transform.Find("Infocanvas").gameObject.SetActive(true);
        }

    }

    public void openLink_bim()
    {
        Application.OpenURL("https://bimtrackbeta.co/en/Login");
    }
    public void openLink_watson()
    {
        Application.OpenURL("https://www.ibm.com/watson/");
    }
}
