﻿using System;
using UnityEngine;

[Serializable]
public class XML_luokka{

    public string tokenBIM;
    public string tokenWatson; //username of Watson
    public string passwordWatson;
    public int HubID;
    public int projectID;
    

    public XML_luokka(string p1, string p2,int p4,int p5, string p6)
    {
        tokenBIM = p1;
        tokenWatson = p2;
        passwordWatson = p6;
        HubID = p4;
        projectID = p5;
    }

    public XML_luokka()
    {
        tokenBIM = "";
        tokenWatson = "";
        passwordWatson = "";
        HubID =0;
        projectID =0;

    }
    public void muokkaa(string p1, string p2,int p4,int p5, string p6)
    {
        tokenBIM = p1;
        tokenWatson = p2;
        passwordWatson = p6;
        HubID = p4;
        projectID = p5;
    }
}
